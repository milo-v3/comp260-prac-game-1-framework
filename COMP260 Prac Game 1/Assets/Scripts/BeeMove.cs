﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed = 2.0f;
	public float maxSpeed = 6.0f;
	public float minTurnSpeed = 90.0f;
	public float maxTurnSpeed = 360.0f;

	public Transform player1;
	public Transform player2;

	public Vector2 heading = Vector3.right;

	public ParticleSystem explosionPrefab;

	private float speed;
	private float turnSpeed;

	void Start () {
		// Make list of all player objects and assigns them as player 1 and 2
		PlayerMove[] playerlist = (PlayerMove[]) FindObjectsOfType(typeof(PlayerMove));
		player1 = playerlist [0].transform;
		player2 = playerlist [1].transform;

		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
	}

	void Update () {
		Vector2 TrueDirection;
		Vector2 direction1 = player1.position - transform.position;
		Vector2 direction2 = player2.position - transform.position;

		if (direction1.magnitude <= direction2.magnitude) {
			TrueDirection = direction1;
		} else {
			TrueDirection = direction2;
		}

		float angle = turnSpeed * Time.deltaTime;

		// Turn right or left
		if (TrueDirection.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} else {
			heading = heading.Rotate (-angle);
		}


		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy(explosion.gameObject, explosion.duration);
	}

	void OnDrawGizmos () {
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading); // draw heading vector in red

		Gizmos.color = Color.yellow;
		Vector2 direction = player1.position - transform.position;
		Gizmos.DrawRay(transform.position, direction); // draw target vector in yellow
	}
}
