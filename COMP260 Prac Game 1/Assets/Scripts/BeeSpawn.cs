﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawn : MonoBehaviour {

	public BeeMove beePrefab;
	public int sBees = 1; // starting number of bees
	private float xMin = -3.0f, yMin = -3.0f;
	private float width = 6.0f, height = 6.0f;

	private int nBees; // current number of bees 
	private float beePeriod; // number of seconds until next bee
	private float minBeePeriod = 2;
	private float maxBeePeriod = 7;

	void Start () {
		//Instatiate a number of bees equal to nBees 
		for (int i = 0; i < sBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			// attach to this object in the heirarchy
			bee.transform.parent = transform;

			bee.gameObject.name = "Bee " + (i+1);

			// move the new clone to a random position within the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}

		nBees = sBees;
		beePeriod = (Mathf.Lerp (minBeePeriod, maxBeePeriod, Random.value));
		Invoke("SpawnBees", beePeriod);
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// Fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}

	public void SpawnBees() {
		nBees = nBees + 1;
		BeeMove bee = Instantiate (beePrefab);

		// attach to this object in the heirarchy
		bee.transform.parent = transform;

		bee.gameObject.name = "Bee " + (nBees);

		// move the new clone to a random position within the bounding rectangle
		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2 (x, y);

		beePeriod = (Mathf.Lerp (minBeePeriod, maxBeePeriod, Random.value));
		Invoke("SpawnBees", beePeriod);
	}
	
	void Update () {
	}
}
