﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public int controlMapping; // Set to the number of the player to assign their input settings

	public float maxSpeed = 5.0f;
	public float acceleration = 2.0f;
	public float brake = 2.0f;
	public float turnSpeed = 30.0f;

	public float destroyRadius = 1.0f;

	private float speed = 0.0f;
	private string AxisX;
	private string AxisY;
	private string fireControl;

	private BeeSpawn beeSpawner;

	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawn> ();

		if (controlMapping == 1) {
			AxisX = "Horizontal"; 
			AxisY = "Vertical";
			fireControl = "Fire1";
		}
		else {
			AxisX = "Horizontal2"; 
			AxisY = "Vertical2";
			fireControl = "Fire2";
		}
	}
	
	// Update is called once per frame
	void Update () {
		// destory bees within radius
		if (Input.GetButtonDown (fireControl)) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		if (brake > maxSpeed) {
			brake = maxSpeed;
		}
			
		// get the input values
		float turn = Input.GetAxis(AxisX);
	
		// turn the car
		transform.Rotate(0.0f, 0.0f, -turn * turnSpeed * speed * Time.deltaTime);
	
		// forwards and backwards
		float forwards = Input.GetAxis(AxisY);
		if (forwards > 0.0f) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0.0f) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} 
		else {
			if (speed > 0.0f) {
				speed = speed - brake * Time.deltaTime;
				if (speed > 0.0f) {
					speed = 0.0f;
				}
			} else if (speed == 0.0f) {
				speed = 0.0f;
			}
			else {
				speed = speed + brake * Time.deltaTime;
				if (speed < 0.0f) {
					speed = 0.0f;
				}
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}